import React from 'react';
import SimpleWeatherPage from './screens/SimpleWeatherPage';

const App: () => React$Node = () => {
  return <SimpleWeatherPage />;
};

export default App;
