import React, { Component } from 'react';
import { Image, Keyboard, StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import DefaultInput from '../components/UI/DefaultInput/DefaultInput';
import ButtonWithBackground from '../components/UI/ButtonWithBackground/ButtonWithBackground';
import WeatherList from '../components/WeatherList/WeatherList';
import HeadingText from '../components/UI/HeadingText/HeadingText';

class SimpleWeatherPage extends Component {
  state = {
    location: 'Lodz',
    items: {},
    requestLocation: 'Łódź',
  };

  static navigationOptions = {
    title: 'Weather',
  };

  componentDidMount() {
    this.searchByLocation();
  }

  updateLocationState = value => {
    this.setState({ location: value });
  };

  searchByLocation = () => {
    fetch(
      `http://api.weatherstack.com/current?access_key=aad66c946d55177684f083a93087a02b&query=${this.state.location}`,
    )
      .then(response => response.json())
      .then(data =>
        this.setState({ items: data.current || [], requestLocation: data.request.query }),
      )
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    console.log(this.state.items.weather_icons && this.state.items.weather_icons[0]);
    return (
      <View>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.inputContainer}>
            <DefaultInput
              placeholder="Location"
              style={styles.input}
              value={this.state.location}
              onChangeText={this.updateLocationState}
              autoCapitalize="none"
            />
            <ButtonWithBackground
              color="#ff9100"
              onPress={this.searchByLocation}
              style={styles.button}
            >
              >
            </ButtonWithBackground>
          </View>
        </TouchableWithoutFeedback>
        {Object.keys(this.state.items).length > 0 && (
          <>
            <HeadingText>{this.state.requestLocation}</HeadingText>
            <HeadingText>Temperature: {this.state.items.temperature}</HeadingText>
            {this.state.items.weather_icons && (
              <Image
                resizeMode="cover"
                source={{ uri: this.state.items.weather_icons[0] }}
                style={styles.weatherImage}
              />
            )}
            <HeadingText>Details</HeadingText>
            <WeatherList
              items={Object.entries(this.state.items).map(([key, value]) => ({
                name: key,
                value: value,
              }))}
            />
          </>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  input: {
    backgroundColor: '#eee',
    borderColor: '#bbb',
    flex: 1,
  },
  button: {
    width: 50,
  },
  weatherImage: {
    marginRight: 8,
    height: 60,
    width: 60,
  },
});

export default SimpleWeatherPage;
