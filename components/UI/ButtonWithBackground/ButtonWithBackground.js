import React from 'react';
import {
  TouchableOpacity,
  TouchableNativeFeedback,
  Text,
  View,
  StyleSheet,
  Platform,
  TextInput,
} from 'react-native';

const ButtonWithBackground = props => {
  const content = (
    <View
      style={[
        styles.button,
        props.style,
        { backgroundColor: props.color },
        props.disabled ? styles.disabled : null,
      ]}
    >
      <Text style={[styles.text, props.disabled ? styles.disabledText : null]}>
        {props.children}
      </Text>
    </View>
  );

  if (props.disabled) {
    return content;
  }

  if (Platform.OS === 'android') {
    return <TouchableNativeFeedback onPress={props.onPress}>{content}</TouchableNativeFeedback>;
  }
  return <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>;
};

const styles = StyleSheet.create({
  button: {
    padding: 10,
    margin: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'black',
  },
  text: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 16,
    textAlign: 'center',
  },
  disabled: {
    backgroundColor: '#eee',
    borderColor: '#aaa',
  },
  disabledText: {
    color: '#aaa',
  },
});

export default ButtonWithBackground;
