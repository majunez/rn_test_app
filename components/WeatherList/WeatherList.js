import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import ListItem from '../ListItem/ListItem';

const WeatherList = props => {
  return (
    <FlatList
      style={styles.listContainer}
      data={props.items}
      renderItem={({ item }) => <ListItem name={item.name} value={item.value} />}
      keyExtractor={item => item.name}
    />
  );
};

const styles = StyleSheet.create({
  listContainer: {
    width: '100%',
  },
});

export default WeatherList;
