import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const listItem = props => (
  <View style={styles.listItem}>
    <Text>
      {props.name}: {props.value}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  listItem: {
    width: '100%',
    padding: 10,
    backgroundColor: '#eee',
    marginBottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default listItem;
